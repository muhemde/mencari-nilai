package com.example.tugas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText


class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var absenNilai : TextInputEditText
    private lateinit var tugasNilai : TextInputEditText
    private lateinit var utsNilai : TextInputEditText
    private lateinit var uasNilai : TextInputEditText
    private lateinit var submitButton : Button
    private lateinit var hasilResult : TextView
    private lateinit var hasilGrade : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        absenNilai = findViewById(R.id.edtxAbsen)
        tugasNilai = findViewById(R.id.edtxTugas)
        utsNilai = findViewById(R.id.edtxUts)
        uasNilai = findViewById(R.id.edtxUas)

        submitButton = findViewById(R.id.buttonSubmit)
        hasilResult = findViewById(R.id.tvResult)
        hasilGrade = findViewById(R.id.tvGrade)

        submitButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.buttonSubmit -> {
                var isEmptyField = false
                val nilaiAbsen = absenNilai.text.toString().trim()
                val nilaiTugas = tugasNilai.text.toString().trim()
                val nilaiUts = utsNilai.text.toString().trim()
                val nilaiUas = uasNilai.text.toString().trim()

                if (nilaiAbsen.isEmpty()){
                    isEmptyField = true
                    absenNilai.error = "Silahkan masukkan absen"
                }else if (nilaiAbsen.toInt() > 100 || nilaiAbsen.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "Range harus diatas 0 hingga 100"
                }

                if (nilaiTugas.isEmpty()){
                    isEmptyField = true
                    tugasNilai.error = "Silahkan masukkan tugas"
                }else if (nilaiTugas.toInt() > 100 || nilaiTugas.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "Range harus diatas 0 hingga 100"
                }

                if (nilaiUts.isEmpty()){
                    isEmptyField = true
                    utsNilai.error = "Silahkan masukkan Uts"
                }else if (nilaiUts.toInt() > 100 || nilaiUts.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "Range harus diatas 0 hingga 100"
                }

                if (nilaiUas.isEmpty()){
                    isEmptyField = true
                    uasNilai.error = "Silahkan masukkan Uas"
                }else if (nilaiUas.toInt() > 100 || nilaiUas.toInt() < 0){
                    isEmptyField = true
                    absenNilai.error = "Range harus diatas 0 hingga 100"
                }

                if (!isEmptyField) {
                    val hitung = HitungNilai(
                        nilaiAbsen.toInt(),
                        nilaiTugas.toInt(),
                        nilaiUts.toInt(),
                        nilaiUas.toInt()
                    )


                    hasilResult.text = "Hasil dari perhitungan adalah ${hitung.HitungNilai()}"
                    hasilGrade.text = "Grade anda : ${hitung.HitungGrade()}"
                }
            }
        }
    }
}

class HitungNilai(
    var absen : Int,
    var tugas : Int,
    var uts : Int,
    var uas : Int
) : getNilai() {

    var fixAbsen = super.getNilai(absen, 10.0)
    var fixTugas = super.getNilai(tugas, 20.0)
    var fixUts = super.getNilai(uts, 30.0)
    var fixUas = super.getNilai(uas, 40.0)

    fun HitungNilai(): Int {
        val hasil =  fixAbsen + fixTugas + fixUts + fixUas
        return hasil
    }
    fun HitungGrade(): String{
        val hasil = fixAbsen + fixTugas + fixUts + fixUas
        when(hasil){
            in 0..50 -> return "E"
            in 50..60 -> return "D"
            in 60..70 -> return "C"
            in 70..80 -> return "B"
            in 80..100 -> return "A"
            else -> return "Nilai Tidak ada dalam grade"
        }
    }
}

open class getNilai(){
    open fun getNilai(nilai: Int, percent: Double): Int {
        val hasil = (nilai * percent) / 100
        return hasil.toInt()
    }
}